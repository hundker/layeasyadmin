
//easyui  tree的json数据格式转换成layui 所需数据格式（待改进）
function TreeHelper(list){
    this.list = list;
}
(function(TreeHelper){
    function getAllChildren(list,item){
        var children = getNextLevelChildren(list,item);
        for(var i=0,ii=children.length;i<ii;i++){
            getAllChildren(list,children[i]);
        }
    }
    //遍历list剩下的数据，找到item的下一层的子节点
    function getNextLevelChildren(list,item){
        var children = [];
        for(var i=list.length-1;i>=0;i--){
            var mid = list[i];
            if(mid.pid === item.id){
                delete mid.pid;
                children.push(mid);
                list.splice(i,1);
            }
           
        }
        if(children.length > 0){
            item.children = children;
        }
        return children;
    }
    TreeHelper.prototype.dataTransferer = function(){
        var list = this.list;
        //根节点必须在数组前面
        list.sort(function(a,b){
            if(a.pid > b.pid){
                return 1;
            }else{
                return -1;
            }
        });
        var item,
            result = [];
        //遍历根节点，递归处理其所有子节点的数据
        //每处理完一个根节点，就将其及其所有子节点从list中删除，加快递归速度
        while(list.length){
            item = list[0];
            list.splice(0,1);
            delete item.pid;
            getAllChildren(list,item);
            result.push(item);
        }
        return result;
    };
})(TreeHelper);

var serverList=[
    {
        "attributes": {
            "url": ""
        },
        "iconCls": "",
        "id": "0a96b97e-05ad-4fa0-8ba5-7dc53ce5fee8",
        "seq": 1,
        "text": "一级菜单",
        "url": ""
    },
    {
        "attributes": {
            "url": "business/demo.html"
        },
        "iconCls": "",
        "id": "66ddf796-85dd-4a52-bcf8-6cca5ef60a37",
        "pid": "0a96b97e-05ad-4fa0-8ba5-7dc53ce5fee8",
        "seq": 1,
        "text": "二级菜单",
        "url": "button.html"
    },
    {
        "attributes": {
            "url": ""
        },
        "iconCls": "",
        "id": "xtgl",
        "seq": 6,
        "text": "系统管理",
        "url": ""
    },
    {
        "attributes": {
            "url": "comn/userManager.html"
        },
        "iconCls": "",
        "id": "yhgl",
        "pid": "xtgl",
        "seq": 1,
        "text": "用户管理",
        "url": "comn/userManager.html"
    },
    {
        "attributes": {
            "url": "comn/menuManager.html"
        },
        "iconCls": "",
        "id": "cdgl",
        "pid": "xtgl",
        "seq": 2,
        "text": "菜单管理",
        "url": "comn/menuManager.html"
    },
    {
        "attributes": {
            "url": "comn/roleManager.html"
        },
        "iconCls": "",
        "id": "jsgl",
        "pid": "xtgl",
        "seq": 3,
        "text": "角色管理",
        "url": "comn/roleManager.html"
    }
]

var data = new TreeHelper(serverList).dataTransferer();
//左侧菜单数据配置
var navs=data;




