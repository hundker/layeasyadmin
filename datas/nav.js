
var nav = [{
	"text": "基本元素",
	////"icon": "fa-cubes",
	"spread": true,
	"children": [{
		"text": "按钮",
		//"icon": "&#xe641;",
		"url": "button.html",
		"children":[{
			"text": "三级按钮",
			//"icon": "&#xe641;",
			"url": "button.html"
		}]
	}, {
		"text": "表单",
		//"icon": "&#xe63c;",
		"url": "form.html"
	}, {
		"text": "表格",
		//"icon": "&#xe63c;",
		"url": "table.html"
	}, {
		"text": "导航",
		//"icon": "&#xe609;",
		"url": "nav.html"
	}, {
		"text": "Tab选项卡",
		////"icon": "&#xe62a;",
		"url": "tab.html"
	}, {
		"text": "辅助性元素",
		//"icon": "&#xe60c;",
		"url": "auxiliar.html"
	}]
}, {
	"text": "组件",
	//"icon": "fa-cogs",
	"spread": false,
	"children": [{
		"text": "Datatable",
		//"icon": "fa-table",
		"url": "begtable.html"
	}, {
		"text": "Navbar组件",
		//"icon": "fa-navicon",
		"url": "navbar.html"
	}]
}, {
	"text": "第三方组件",
	//"icon": "&#x1002;",
	"spread": false,
	"children": [{
		"text": "iCheck组件",
		//"icon": "fa-check-square-o",
		"url": "icheck.html"
	}]
}, {
	"text": "地址本",
	//"icon": "fa-address-book",
	"url": "",
	"spread": false,
	"children": [{
		"text": "Github",
		//"icon": "fa-github",
		"url": "https://www.github.com/"
	}, {
		"text": "QQ",
		//"icon": "fa-qq",
		"url": "http://www.qq.com/"
	}, {
		"text": "Fly社区",
		//"icon": "&#xe609;",
		"url": "http://fly.layui.com/"
	}, {
		"text": "新浪微博",
		//"icon": "fa-weibo",
		"url": "http://weibo.com/"
	}]
}, {
	"text": "这是一级导航",
	//"icon": "fa-stop-circle",
	"url": "https://www.baidu.com",
	"spread": false
}];