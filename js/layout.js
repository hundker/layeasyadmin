//实现三级导航，第一级导航横置于页面上端
layui.config({
	base: 'js/'
}).use(['element', 'layer', 'navbar', 'tab'], function() {
	var element = layui.element()
	$ = layui.jquery,
		layer = layui.layer,
		navbar = layui.navbar(),
		tab = layui.tab({
			elem: '.layout-nav-card' //设置选项卡容器
		});

	//iframe自适应
	$(window).on('resize', function() {
		var $content = $('.layout-nav-card .layui-tab-content');
		$content.height($(this).height() - 147);
		$content.find('iframe').each(function() {
			$(this).height($content.height());
		});
	}).resize();

	var $menu = $('#menu');
	$menu.find('li.layui-nav-item').each(function() {
		var $this = $(this);
		//绑定一级导航的点击事件s
		$this.on('click', function() {
			//获取设置的模块ID
		var id = $this.find('a').data('module-id');
          var data;
          	$.ajax({
          	dataType : 'json',
          	url:"datas/menus.json",
          	async:false,
          	success : function(obj) {
              var transData=new TreeHelper(obj).dataTransferer();     
              data = transData;	
                }
          	});
			 var navbarData;
			 for(var i=0;i<data.length;i++){
			 	if(data[i].id==id){
			 	 navbarData=data[i].children;
			 	}
			 }
			//设置navbar
			navbar.set({
				elem: '#side', //存在navbar数据的容器ID
				data:navbarData
			});
			//渲染navbar
			navbar.render();
			//监听点击事件
			navbar.on('click(side)', function(data) {
				tab.tabAdd(data.field);
			});
		});

	});
	//默认点击第一项
	$('.beg-layout-menu').find('a[data-module-id=100]').click();

	$('.beg-layout-side-toggle').on('click', function() {
		var sideWidth = $('.beg-layout-side').width();
		if(sideWidth === 200) {
			$('.beg-layout-body').animate({
				left: '0'
			});
			$('.beg-layout-footer').animate({
				left: '0'
			});
			$('.beg-layout-side').animate({
				width: '0'
			});
		} else {
			$('.beg-layout-body').animate({
				left: '200px'
			});
			$('.beg-layout-footer').animate({
				left: '200px'
			});
			$('.beg-layout-side').animate({
				width: '200px'
			});
		}
	});
});